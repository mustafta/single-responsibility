package part1;
import java.util.Date;
public class Employee {
    private String employeeId;
    private String name;
    private Date joinDate;
    public Employee(String employeeId, String name) {
        this.employeeId = employeeId;
        this.name = name;
            }

    public String getEmployeeId() {
        return employeeId;
    }

    public String getName() {
        return name;
    }

    public Date getDate() {
        return joinDate;
    }
    public boolean isPromotionDueThisYear(){
   return true;
   } 
    @Override
    public String toString(){
        return "Employee Name: " + getName() + "Employee ID" + getEmployeeId();
    }
}
