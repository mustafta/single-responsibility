
package part1;
public class Demo {
    public static void main(String[] args) {
        Employee employee = new Employee("6575", " John ");
        boolean thisYearPromotionValid = employee.isPromotionDueThisYear();
        IncomeTaxCalculator tax = new IncomeTaxCalculator();
        double YearIncomeTax = tax.incomeTaxYear(employee);
        System.out.println(employee + "\t" +"  This Year Promotion " + thisYearPromotionValid + "\t" + "Income Tax: $" + YearIncomeTax);
    }
    }
